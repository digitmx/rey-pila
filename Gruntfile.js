'use strict';
module.exports = function(grunt) {

    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

		watch: {
		    js: {
		        files: ['js/functions.js'],
		        tasks: ['concat:js','uglify']
    		},
    		css: {
	    		files: ['style.css'],
		        tasks: ['concat:css','cssmin']
    		},
    		php: {
	    		files: ['*.php','page-templates/*.php','includes/*.php'],
		        tasks: ['copy']
    		}
		},

        copy: {
		  	main: {
		    	files: [
					// includes files within path and its sub-directories
					{expand: true, src: ['style.css'], dest: 'dist/'},
					{expand: true, src: ['*.php'], dest: 'dist/'},
					{expand: true, src: ['fonts/roboto/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/din/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/*'], dest: 'dist/'},
					{expand: true, src: ['css/ajax-loader.gif'], dest: 'dist/'},
					{expand: true, src: ['css/fonts/*'], dest: 'dist/'},
					{expand: true, src: ['screenshot.png'], dest: 'dist/'},
					{expand: true, src: ['js/html5shiv-printshiv.min.js'], dest: 'dist/'},
					{expand: true, src: ['js/respond.min.js'], dest: 'dist/'},
					{expand: true, src: ['page-templates/*'], dest: 'dist/'},
					{expand: true, src: ['includes/*'], dest: 'dist/'},
				],
		  	},
		},

        concat: {
            css: {
                src: [
                    'css/materialize.min.css','css/font-awesome.min.css','css/normalize.css','css/slick.css','css/slick-theme.css','style.css'
                ],
                dest: 'combined/combined.css'
            },
            js: {
                src: [
                    'js/jquery-3.1.1.min.js','js/materialize.min.js','js/validation/jquery.validate.min.js','js/validation/additional-methods.min.js','js/validation/localization/messages_es.min.js','js/slick.min.js','js/functions.js'
                ],
                dest: 'combined/combined.js'
            }
        },

        cssmin: {
            css: {
                src: 'combined/combined.css',
                dest: 'dist/css/app.css'
            },
            css_local: {
                src: 'combined/combined.css',
                dest: 'css/app.css'
            }
        },

        uglify: {
            js: {
                files: {
                    'dist/js/app.js': ['combined/combined.js']
                }
            },
            js_local: {
                files: {
                    'js/app.js': ['combined/combined.js']
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true
                },
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'dist/img/'
                },
                {
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'img/'
                }]
            }
        },

        clean: ["dist/"]

    });

    // register task
    grunt.registerTask('default', ['clean', 'imagemin', 'concat', 'cssmin', 'uglify', 'copy']);

};