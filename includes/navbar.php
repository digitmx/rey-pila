		<?php
		    global $post;
		    $post_slug = $post->post_name;
		    if (!$post_slug) { $post_slug = 'home'; }
			
			if ($post_slug == 'home') 
			{
				create_landing_menu( 'landing-menu' ); 
			}
			else
			{
				create_site_menu( 'site-menu' );
			}
		?>
		<div class="navbar-fixed hide-on-med-and-down" id="nav-mailing">
			<nav class="white centered">
				<div class="nav-wrapper" id="mc_embed_signup">
					<form action="//reypila.us14.list-manage.com/subscribe/post-json?u=a68dc2f44e7fd847725e74dc2&amp;id=17421f7c4e&amp;c=?" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<div class="input-field">
							<span class="din font22 black-text"><?php _e("JOIN MAILING LIST","reypila_v1"); ?></span>
						</div>
						<div class="input-field">
							<input type="text" value="" placeholder="<?php _e("Name","reypila_v1"); ?>" name="FNAME" class="din font22 black-text" id="mce-FNAME" style="width: 100px;">
	        			</div>
	        			<div class="input-field">
							<input type="text" value="" placeholder="<?php _e("Last Name","reypila_v1"); ?>" name="LNAME" class="din black-text font22" id="mce-LNAME" style="width: 100px;">
	        			</div>
	        			<div class="input-field">
							<input type="email" value="" placeholder="<?php _e("Email","reypila_v1"); ?>" name="EMAIL" class="required email din black-text font22" id="mce-EMAIL" style="width: 200px;">
	        			</div>
	        			<div class="input-field">
							<input type="text" value="" placeholder="<?php _e("Country","reypila_v1"); ?>" name="MMERGE5" class="din black-text font22" id="mce-MMERGE5" style="width: 100px;">
	        			</div>
	        			<div class="input-field">
		        			<input type="submit" value="<?php _e("Submit","reypila_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="button btn-flat btn-block black-text din font22">
		        		</div>
	        			<div id="mce-responses" class="clear">
		        			<div class="response" id="mce-error-response" style="display:none"></div>
		        			<div class="response" id="mce-success-response" style="display:none"></div>
		        		</div>
		        		<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a68dc2f44e7fd847725e74dc2_17421f7c4e" tabindex="-1" value=""></div>
	      			</form>
	    		</div>
	  		</nav>
		</div>