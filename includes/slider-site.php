		
		<?php $slider = get_field("slider_site", "option"); ?>
		<div class="container-fluid" id="slider">
	        <div class="slider">
			    <ul class="slides">
			    	<?php foreach ($slider as $item) { $mobile = $item['mobile']; if (!$mobile) { $mobile = $item['image']; } ?>
					<li>
			        	<img class="hide-on-med-and-down" src="<?php echo $item['image']; ?>"> 
			        	<img class="hide-on-large-only" src="<?php echo $mobile; ?>">
						<div class="caption <?php echo $item['position']; ?>-align">
							<h3 class="din font70 white-text"><?php echo $item['title']; ?></h3>
							<h5 class="helvetica font18 white-text"><?php echo $item['decription']; ?></h5>
			        	</div>
					</li> 
					<?php } ?>
			    </ul>
			 </div>
      	</div>