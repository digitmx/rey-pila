  		<?php if (!is_home()) { ?>
  		<div class="container">
	  		<div class="row">
		  		<div class="col s12 m12 l12">
			  		<div class="space60"></div>
			  		<div class="centered">
				  		<span class="bold din font16 black-text">© 2017 Rey Pila. All rights reserved.</span>
				  	<div class="space40"></div>
				</div>
	      	</div>
  		</div>
  		<?php } ?>
		
		<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
		<input type="hidden" id="kichink" name="kichink" value="<?php the_field("kichink_site", "option"); ?>" />
		<input type="hidden" id="msg_thanks" name="msg_thanks" value="<?php _e("Thanks for subscribing","reypila_v1"); ?>" />
		
		<?php wp_footer(); ?>

	</body>
</html>