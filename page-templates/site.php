<?php /* Template Name: Site */ ?>

<?php get_header(); ?>
		
		<!-- NAVBAR -->
		<?php get_template_part("includes/navbar"); ?>
		
		<!-- SLIDER -->
		<?php get_template_part("includes/slider", "site"); ?>
      	
      	<!-- NEWS -->
      	<div class="container" id="news">
	      	<div class="row">
		      	<div class="col s12 m12 l12">
			      	<div class="space40"></div>
			      	<div class="centered">
				      	<span class="din font48 black-text"><?php _e("NEWS","reypila_v1"); ?></span>
			      	</div>
			      	<div class="space40"></div>
		      	</div>
		    </div>
	      	  
		    <?php 
			    //Query News
			    $args = array(
					'posts_per_page'   => 3,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'suppress_filters' => false 
				);
				$query = new WP_Query( $args );
			?>
			<?php foreach ($query->posts as $posts) { //Proccess News ?>
		    <div class="row">
			    <div class="col s12 m12 l6">
				    <img class="responsive-img" src='<?php echo get_the_post_thumbnail_url( $posts->ID, $size = 'full' ); ?>'/>
				</div>
			  	<div class="col s12 m12 l6">
					<div class="space40"></div>
					<span class="din font24 black-text centeres"><?php echo $posts->post_title; ?></span>
		      	  	<div class="space40"></div>
		      	  	<span class="helvetica font14 black-text centered"><?php echo $posts->post_excerpt; ?></span>
		      	  	<div class="space40"></div>
		      	  	<a href="<?php echo get_the_permalink($posts->ID); ?>" class="waves-effect waves-light btn-flat btn-site-w helvetica font14"><?php _e("VIEW POST","reypila_v1"); ?></a>
		      	</div>
		    </div>
		    <?php } ?>
		      	
	      	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<div class="centered">
				    	<a href="<?php bloginfo('url'); ?>/news/" class="waves-effect waves-light btn-flat btn-site-b helvetica font14"><?php _e("VIEW MORE","reypila_v1"); ?></a>
				    </div>
		      	</div>
	      	</div>
      	</div>
      	
      	<!-- LIVE -->  
      	<?php $color = get_field("color_live_site", "option"); ?>
      	<?php $percentage = get_field("percentage_live_site", "option"); ?>
      	<?php $songkick_code = get_field("songkick_live_site", "option"); ?>
      	<?php $live_cover = get_field("live_site", "option"); ?>
      	<?php $rgba = hex2rgba($color, $percentage); ?>
      	<div class="container-fluid" id="live">
	      	<div class="row">
		      	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<span class="din font48 black-text centered block"><?php _e("LIVE","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
	      	<div class="parallax-container">
		  		<div class="parallax">
			  		<div class="cover" style="background-color: <?php echo $rgba; ?>;"></div>
			  		<img src="<?php echo $live_cover; ?>">
			  	</div>
			  	<div class="row">
				  	<div class="col s12 m12 l6 offset-l3">
					  	<div class="space40"></div>
					  	<div class="songkick"><?php the_field("songkick_live_site", "option") ?></div>
				  	</div>
			  	</div>
		  	</div>
      	</div>
      	
      	<!-- MUSIC -->  
      	<?php $background_albums = get_field("background_albums_music", "option"); ?>
      	<?php $background_singles = get_field("background_singles_music", "option"); ?>
      	<?php $background_videos = get_field("background_videos_music", "option"); ?>
      	<div class="container-fluid" id="music">
	    	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<span class="din font48 black-text centered block"><?php _e("MUSIC","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
	      	<div class="parallax-container">
		  		<div class="parallax">
			  		<div class="cover"></div>
			  		<img src="<?php echo $background_albums; ?>">
			  	</div>
			  	<div class="row">
				  	<div class="col s12 m12 l6 offset-l3">
					  	<div class="space40"></div>
					  	<div class="row">
					  		<div class="col s12">
					  			<ul class="tabs transparent">
					  				<li class="tab col s4"><a rel="albums" class="helvetica font18 white-text bold active" href="#div_albums"><?php _e("ALBUMS","reypila_v1"); ?></a></li>
					  				<li class="tab col s4"><a rel="singles" class="helvetica font18 white-text bold" href="#div_singles"><?php _e("SINGLES","reypila_v1"); ?></a></li>
					  				<li class="tab col s4"><a rel="videos" class="helvetica font18 white-text bold" href="#div_videos"><?php _e("VIDEOS","reypila_v1"); ?></a></li>
					  			</ul>
					  		</div>
							<div id="div_albums" class="col s12">
								<?php $albums = get_field("albums_music", "option"); ?>
								<?php foreach ($albums as $album) { ?>
								<div>
									<center>
									<div class="card">
								    	<div class="card-image waves-effect waves-block waves-light">
											<img class="activator" src="<?php echo $album['cover']; ?>">
										</div>
										<div class="card-content">
											<span class="card-title activator din font30 blok-text block left-text"><?php echo $album['title']; ?><i class="material-icons right">play_circle_outline</i></span>
											<p class="helvetica font16 black-text block left-text"><?php echo $album['year']; ?></p>
										</div>
								    	<div class="card-reveal">
											<span class="card-title din font30 blok-text block left-text"><?php echo $album['title']; ?><i class="material-icons right font40">close</i></span>
											<div class="col s12">
											  	<div class="space20"></div>
											  	<div class="centered">
												  	<?php if ($album['year']) { ?>
													<span class="helvetica font16 black-text block left-text"><?php echo $album['year']; ?></span>
													<?php } ?>
													<?php if ($album['itunes'] || $album['disc']) { ?>
														<div class="space20"></div>
														<span class="helvetica font16 black-text block left-text"><?php _e("Buy on", "reypila_v1"); ?></span>
														<?php if ($album['itunes']) { ?>
														<div class="space10"></div>
														<a href="<?php echo $album['itunes']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica black-text font14 left-text">
															<div class="icon">
																<svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg>
															</div>
															<?php _e("iTunes","reypila_v1"); ?>
														</a>
														<?php } ?>
														<?php if ($album['disc']) { ?>
														<div class="space10"></div>
														<a href="<?php echo $album['disc']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica black-text font14 left-text">
															<div class="icon">
																<svg id="disc" data-name="disc" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><title>cd</title><path d="M25,0A25,25,0,1,0,50,25,25,25,0,0,0,25,0Zm0,45.8A20.8,20.8,0,1,1,45.8,25,20.7,20.7,0,0,1,25,45.8Z"/><path d="M25,14.8A10.2,10.2,0,1,0,35.2,25,10.3,10.3,0,0,0,25,14.8ZM25,31a6,6,0,1,1,6-6A6,6,0,0,1,25,31Z"/><path d="M25,22.6A2.4,2.4,0,1,0,27.4,25,2.4,2.4,0,0,0,25,22.6Z"/></svg>
															</div>
															<?php _e("CD / Vinyl","reypila_v1"); ?>
														</a>
														<?php } ?>
													<?php } ?>
											  	</div>
										  	</div>
										  	<div class="col s12">
											  	<div class="video-container">
												  	<iframe src="https://embed.spotify.com/?uri=spotify:album:<?php echo $album['spotify']; ?>"  height="413" frameborder="0" allowtransparency="true"></iframe>
											  	</div>
										  	</div>
								    	</div>
									</div>
									</center>
								</div>	
								<?php } ?>
							</div>
							<div id="div_singles" class="col s12">
								<?php $singles = get_field("singles_music", "option"); ?>
								<?php foreach ($singles as $single) { ?>
								<div>
									<center>
									<div class="card">
								    	<div class="card-image waves-effect waves-block waves-light">
											<img class="activator" src="<?php echo $single['cover']; ?>">
										</div>
										<div class="card-content">
											<span class="card-title activator din font30 blok-text block left-text"><?php echo $single['title']; ?><i class="material-icons right">play_circle_outline</i></span>
											<p class="helvetica font16 black-text block left-text"><?php echo $single['year']; ?></p>
										</div>
								    	<div class="card-reveal">
											<span class="card-title din font30 blok-text block left-text"><?php echo $single['title']; ?><i class="material-icons right font40">close</i></span>
											<div class="col s12">
											  	<div class="space20"></div>
											  	<div class="centered">
												  	<?php if ($single['year']) { ?>
													<span class="helvetica font16 black-text block left-text"><?php echo $single['year']; ?></span>
													<?php } ?>
													<?php if ($single['itunes'] || $single['disc']) { ?>
														<div class="space20"></div>
														<span class="helvetica font16 black-text block left-text"><?php _e("Buy on", "reypila_v1"); ?></span>
														<?php if ($single['itunes']) { ?>
														<div class="space10"></div>
														<a href="<?php echo $single['itunes']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica black-text font14 left-text">
															<div class="icon">
																<svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg>
															</div>
															<?php _e("iTunes","reypila_v1"); ?>
														</a>
														<?php } ?>
														<?php if ($single['disc']) { ?>
														<div class="space10"></div>
														<a href="<?php echo $single['disc']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica black-text font14 left-text">
															<div class="icon">
																<svg id="disc" data-name="disc" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><title>cd</title><path d="M25,0A25,25,0,1,0,50,25,25,25,0,0,0,25,0Zm0,45.8A20.8,20.8,0,1,1,45.8,25,20.7,20.7,0,0,1,25,45.8Z"/><path d="M25,14.8A10.2,10.2,0,1,0,35.2,25,10.3,10.3,0,0,0,25,14.8ZM25,31a6,6,0,1,1,6-6A6,6,0,0,1,25,31Z"/><path d="M25,22.6A2.4,2.4,0,1,0,27.4,25,2.4,2.4,0,0,0,25,22.6Z"/></svg>
															</div>
															<?php _e("CD / Vinyl","reypila_v1"); ?>
														</a>
														<?php } ?>
													<?php } ?>
											  	</div>
										  	</div>
										  	<div class="col s12">
											  	<div class="video-container">
												  	<iframe src="https://embed.spotify.com/?uri=spotify:album:<?php echo $single['spotify']; ?>"  height="413" frameborder="0" allowtransparency="true"></iframe>
											  	</div>
										  	</div>
								    	</div>
									</div>
									</center>
								</div>	
								<?php } ?>
							</div>
							<?php $videos = get_field("videos_music", "option"); ?>
							<div id="div_videos" class="col s12">
								<?php foreach ($videos as $video) { ?>
								<div>
									<center>
										<div class="card transparent">
								            <div class="card-content white-text">
								              <span class="card-title din font30 blok-text block left-text"><?php echo $video['title']; ?></span>
								              <div class="video-container">
											    	<iframe width="1280" height="720" src="//www.youtube.com/embed/<?php echo $video['youtube']; ?>?rel=0&autoplay=0&controls=1&disablekb=0&fs=1&iv_load_policy=3&showinfo=0" frameborder="0" allowfullscreen></iframe>
											    </div>
								            </div>
								        </div>
									</center>
								</div>
								<?php } ?>
							</div>
					  	</div>  	
				  	</div>
			  	</div>
			  	<!--
			  	<div class="row">
				  	<div class="col s12 m12 l6 offset-l3">
					  	<div class="space40"></div>
					  	<div class="row" id="menu">
						  	<div class="col s4 centered">
							  	<a href="#" rel="albums" background="<?php echo $background_albums; ?>" class="helvetica font18 white-text bold underline"><?php _e("ALBUMS","reypila_v1"); ?></a>
						  	</div>
						  	<div class="col s4 centered">
							  	<a href="#" rel="singles" background="<?php echo $background_singles; ?>" class="helvetica font18 magnesium-text"><?php _e("SINGLES","reypila_v1"); ?></a>
						  	</div>
						  	<div class="col s4 centered">
							  	<a href="#" rel="videos" background="<?php echo $background_videos; ?>" class="helvetica font18 magnesium-text"><?php _e("VIDEOS","reypila_v1"); ?></a>
						  	</div>
					  	</div>
				  	</div>
			  	</div>
			  	<div class="row">
				  	<div class="container">
					  	<div class="row" id="sections">
						  	<?php $albums = get_field("albums_music", "option"); ?>
						  	<div class="col s12 m12 l12 section" id="albums">
								<div class="row">
									<?php foreach ($albums as $album) { ?>
									<div class="col s12 m6 l6">
										<div class="row">
											<div class="col s12 m6 l6" id="cover">
												<img src="<?php echo $album['cover']; ?>" class="responsive-img" />
											</div>
											<div class="col s12 m6 l6" id="text">
												<div class="space40 hide-on-med-and-down"></div>
												<span class="din font30 white-text block"><?php echo $album['title']; ?></span>
												<?php if ($album['year']) { ?>
												<span class="helvetica font16 white-text block"><?php echo $album['year']; ?></span>
												<?php } ?>
												<div class="space10"></div>
												<a href="#" background="<?php echo $album['cover']; ?>" rel="<?php echo sanitize_title($album['title']); ?>" class="waves-effect waves-light btn-flat btn-site-ww helvetica font14"><?php _e("VIEW ALBUM","reypila_v1"); ?></a>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
							<?php $singles = get_field("singles_music", "option"); ?>
							<div class="col s12 m12 l12 section" id="singles" style="display: none;">
							  	<div class="row">
									<?php foreach ($singles as $single) { ?>
									<div class="col s12 m6 l6">
										<div class="row">
											<div class="col s12 m6 l6" id="cover">
												<img src="<?php echo $single['cover']; ?>" class="responsive-img" />
											</div>
											<div class="col s12 m6 l6" id="text">
												<div class="space40 hide-on-med-and-down"></div>
												<span class="din font30 white-text block"><?php echo $single['title']; ?></span>
												<?php if ($album['year']) { ?>
												<span class="helvetica font16 white-text block"><?php echo $single['year']; ?></span>
												<?php } ?>
												<div class="space10"></div>
												<a href="#" background="<?php echo $single['cover']; ?>" rel="<?php echo sanitize_title($single['title']); ?>" class="waves-effect waves-light btn-flat btn-site-ww helvetica font14"><?php _e("VIEW SINGLE","reypila_v1"); ?></a>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
							<?php $videos = get_field("videos_music", "option"); ?>
						  	<div class="col s12 m12 l12 section" id="videos" style="display: none;">
							  	<div class="row">
									<?php foreach ($videos as $video) { ?>
									<div class="col s12 m6 l4">
										<a href="#video_<?php echo sanitize_title($video['title']); ?>">
											<span class="din font30 white-text block"><?php echo $video['title']; ?></span>
											<div class="space10"></div>
											<img src="https://img.youtube.com/vi/<?php echo $video['youtube']; ?>/mqdefault.jpg" class="responsive-img" />
										</a>
										<div id="video_<?php echo sanitize_title($video['title']); ?>" class="modal black">
									    	<a href="#" class="close-modal white-text"><i class="fa fa-times fa-2x" aria-hidden="true"></i></a>
									    	<div class="modal-content">
												<div class="video-container">
											    	<iframe width="1280" height="720" src="//www.youtube.com/embed/<?php echo $video['youtube']; ?>?rel=0&autoplay=0&controls=0&disablekb=0&fs=0&iv_load_policy=3&showinfo=0" frameborder="0" allowfullscreen></iframe>
											    </div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
						  	</div>
						  	<?php foreach ($albums as $album) { ?>
						  	<div class="col s12 m12 l12 section" id="<?php echo sanitize_title($album['title']); ?>" style="display: none;">
							  	<div class="row">
								  	<div class="col s12 m6 l3">
									  	<img src="<?php echo $album['cover']; ?>" class="responsive-img" />
									  	<div class="space20"></div>
									  	<div class="centered">
										  	<span class="din font30 white-text block"><?php echo $album['title']; ?></span>
										  	<?php if ($album['year']) { ?>
											<span class="helvetica font16 white-text block"><?php echo $album['year']; ?></span>
											<?php } ?>
											<?php if ($album['itunes'] || $album['disc']) { ?>
												<div class="space20"></div>
												<span class="helvetica font16 white-text block"><?php _e("Buy on", "reypila_v1"); ?></span>
												<?php if ($album['itunes']) { ?>
												<div class="space10"></div>
												<a href="<?php echo $album['itunes']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica font14">
													<div class="icon">
														<svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg>
													</div>
													<?php _e("iTunes","reypila_v1"); ?>
												</a>
												<?php } ?>
												<?php if ($album['disc']) { ?>
												<div class="space10"></div>
												<a href="<?php echo $album['disc']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica font14">
													<div class="icon">
														<svg id="disc" data-name="disc" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><title>cd</title><path d="M25,0A25,25,0,1,0,50,25,25,25,0,0,0,25,0Zm0,45.8A20.8,20.8,0,1,1,45.8,25,20.7,20.7,0,0,1,25,45.8Z"/><path d="M25,14.8A10.2,10.2,0,1,0,35.2,25,10.3,10.3,0,0,0,25,14.8ZM25,31a6,6,0,1,1,6-6A6,6,0,0,1,25,31Z"/><path d="M25,22.6A2.4,2.4,0,1,0,27.4,25,2.4,2.4,0,0,0,25,22.6Z"/></svg>
													</div>
													<?php _e("CD / Vinyl","reypila_v1"); ?>
												</a>
												<?php } ?>
											<?php } ?>
									  	</div>
								  	</div>
								  	<div class="col s12 m6 l9">
									  	<div class="video-container">
										  	<iframe src="https://embed.spotify.com/?uri=spotify:album:<?php echo $album['spotify']; ?>"  height="413" frameborder="0" allowtransparency="true"></iframe>
									  	</div>
								  	</div>
							  	</div>
						  	</div>
						  	<?php } ?>
						  	<?php foreach ($singles as $single) { ?>
						  	<div class="col s12 m12 l12 section" id="<?php echo sanitize_title($single['title']); ?>" style="display: none;">
							  	<div class="row">
								  	<div class="col s12 m6 l3">
									  	<img src="<?php echo $single['cover']; ?>" class="responsive-img" />
									  	<div class="space20"></div>
									  	<div class="centered">
										  	<span class="din font30 white-text block"><?php echo $single['title']; ?></span>
										  	<?php if ($single['year']) { ?>
											<span class="helvetica font16 white-text block"><?php echo $single['year']; ?></span>
											<?php } ?>
											<?php if ($single['itunes']) { ?>
												<div class="space20"></div>
												<span class="helvetica font16 white-text block"><?php _e("Buy on", "reypila_v1"); ?></span>
												<div class="space10"></div>
												<a href="<?php echo $single['itunes']; ?>" target="_blank" class="normal-text waves-effect waves-light btn-site-wb btn-block helvetica font14">
													<div class="icon">
														<svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg>
													</div>
													<?php _e("iTunes","reypila_v1"); ?>
												</a>
											<?php } ?>
									  	</div>
								  	</div>
								  	<div class="col s12 m6 l9">
									  	<div class="video-container">
										  	<iframe src="https://embed.spotify.com/?uri=spotify:track:<?php echo $single['spotify']; ?>"  height="413" frameborder="0" allowtransparency="true"></iframe>
									  	</div>
								  	</div>
							  	</div>
						  	</div>
						  	<?php } ?>
					  	</div>
				  	</div>
			  	</div>
			  	-->
		  	</div>
      	</div>
      	  
      	<!-- STORE -->
      	<?php if (get_field("kichink_site", "option")) { ?>
      	<div class="container" id="store">
	    	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<span class="din font48 black-text centered block"><?php _e("STORE","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
	      	
	      	<?php 
			    //Query Products
			    $args = array(
					'posts_per_page'   => 6,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'product',
					'post_status'      => 'publish',
					'suppress_filters' => true 
				);
				$products = new WP_Query( $args );
			?>
			<div class="row">
				<?php foreach ($products->posts as $product) { //Proccess Products ?>
				<div class="col s12 m6 l4">
					<a href="<?php echo get_permalink($product->ID); ?>">
						<img class="responsive-img block" src="<?php echo get_the_post_thumbnail_url($product->ID); ?>" />
						<div class="space10"></div>
						<span class="din font24 black-text block"><?php the_field("price", $product->ID); ?></span>
						<span class="din font16 magnesium-text block"><?php echo $product->post_title; ?></span>
						<div class="space20"></div>
					</a>
				</div>
				<?php } ?>
			</div> 	
		      	
	      	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<div class="centered">
				    	<a href="<?php bloginfo('url'); ?>/store/" class="waves-effect waves-light btn-flat btn-site-b helvetica font14"><?php _e("VIEW MORE","reypila_v1"); ?></a>
				    </div>
		      	</div>
	      	</div>
      	</div>
      	<?php } ?>
      	
      	<hr />
      	
      	<!-- CONTACTS -->
		<?php get_template_part("includes/contacts"); ?>
				
<?php get_footer(); ?>