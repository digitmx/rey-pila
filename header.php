<?php
    global $post;
    $post_slug = $post->post_name;
    if (!$post_slug) { $post_slug = 'home'; $bg_landing = get_field("cover_landing", "option"); }
    if (is_single()) { $post_slug = $post->post_type; } 
    if (is_page('about')) { $post_slug = 'new'; } 
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?>">
		<title><?php bloginfo('name'); ?></title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_directory"); ?>/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo("template_directory"); ?>/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo("template_directory"); ?>/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo("template_directory"); ?>/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo("template_directory"); ?>/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php bloginfo("template_directory"); ?>/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<script>(function(h){h.className = h.className.replace('no-js', 'js')})(document.documentElement)</script>
		<!-- HTML5 shim and Respond.js IE8 support for HTML5 elements and media queries. -->
		<!--[if lt IE 9]>
        <script src="<?php bloginfo("template_directory"); ?>/js/html5shiv-printshiv.min.js"></script>
        <script src="<?php bloginfo("template_directory"); ?>/js/respond.min.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-96193693-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	</head>
	<body class="<?php echo $post_slug; ?>"<?=($post_slug=='home') ? ' style="background-image: url('.$bg_landing.');"' : '';?>>
		<div class="se-pre-con"></div>

		<!--[if lt IE 8>
			<p class="browsehappy">
				You are using an <strong>outdated</strong> browser.
				Please <a href="http://browsehappy.com/">upgrade your browser</a>
				to improve your experience.
			</p>
	    <![endif]-->