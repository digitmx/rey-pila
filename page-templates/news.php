<?php /* Template Name: News */ ?>

<?php get_header(); ?>

		<!-- NAVBAR -->
		<?php get_template_part("includes/navbar"); ?>
		
		<!-- NEWS -->
      	<div class="container" id="news">
	      	<div class="row">
		      	<div class="col s12 m12 l12">
			      	<div class="space40"></div>
			      	<div class="centered">
				      	<span class="din font48 black-text"><?php _e("NEWS","reypila_v1"); ?></span>
			      	</div>
			      	<div class="space40"></div>
		      	</div>
		    </div>
		    
		    <?php 
			    //Query News
			    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			    $args = array(
					'posts_per_page'   => 10,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'paged'			   => $paged,
					'suppress_filters' => false 
				);
				$query = new WP_Query( $args );
			?>
			<?php while ( $query->have_posts() ) : $query->the_post(); //Proccess News ?>
		    <div class="row">
			    <div class="col s12 m12 l6">
				    <img class="responsive-img" src='<?php echo get_the_post_thumbnail_url( $query->ID, $size = 'full' ); ?>'/>
				</div>
			  	<div class="col s12 m12 l6">
					<div class="space40"></div>
					<span class="din font24 black-text"><?php the_title(); ?></span>
		      	  	<div class="space40"></div>
		      	  	<span class="helvetica font14 black-text"><?php the_excerpt(); ?></span>
		      	  	<div class="space40"></div>
		      	  	<a href="<?php the_permalink(); ?>" class="waves-effect waves-light btn-flat btn-site-w helvetica font14"><?php _e("VIEW POST","reypila_v1"); ?></a>
		      	</div>
		    </div>
		    <?php endwhile; wp_reset_postdata(); ?>
		      	
	      	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			    	<div class="navigation">
						<div class="pull-left">
							<?php previous_posts_link( 'Previous' ); ?>
						</div>
						<div class="pull-right">
							<?php next_posts_link( 'Next', $query->max_num_pages ); ?>
						</div>
					</div>
		      	</div>
	      	</div>
	      	
      	</div>
      	
      	<hr />
      	
      	<!-- CONTACTS -->
		<?php get_template_part("includes/contacts"); ?>
		
<?php get_footer(); ?>