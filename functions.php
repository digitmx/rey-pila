<?php

	//Remove Enqueue Scripts
	remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page();

	}
	
	add_filter('acf/settings/default_language', 'my_acf_settings_default_language');
 
	function my_acf_settings_default_language( $language ) {
	 
	    return 'en';
	    
	}
	
	add_filter('acf/settings/current_language', 'my_acf_settings_current_language');
 
	function my_acf_settings_current_language( $language ) {
	 
	    return 'es';
	    
	}
	
	//Register Menus
	function register_my_menus() {
	  register_nav_menus(
	    array(
	      'landing-menu' => __( 'Landing Menu' ),
	      'site-menu' => __( 'Site Menu' )
	    )
	  );
	}
	add_action( 'init', 'register_my_menus' );
	
	//Custom Landing Menu
	function create_landing_menu( $theme_location ) 
	{
		//Check Theme Location
	    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) 
	    {
		    //Check Languages
		    $languages = icl_get_languages('skip_missing=1'); 
		    $es = isset($languages['es']['url']) ? $languages['es']['url'] : '#'; 
		    $en = isset($languages['en']['url']) ? $languages['en']['url'] : '#';
		    
		    //Buit Navbar
	        $menu_list  = '<div class="navbar-fixed">' ."\n";
	        $menu_list .= '	<nav class="transparent">' ."\n";
	        $menu_list .= '		<div class="nav-wrapper container">' ."\n";
	        
	        //Add Logo
	        $menu_list .= '			<a href="' . get_bloginfo('url').'" class="brand-logo">' . "\n";
	        $menu_list .= '				<img src="' . get_bloginfo("template_directory") . '/img/logo-landing.png"/>' ."\n";
	        $menu_list .= '			</a>' ."\n";
	        
	        //Read Menu Items 
	        $menu = get_term( $locations[$theme_location], 'nav_menu' );
	        $menu_items = wp_get_nav_menu_items($menu->term_id);
	 
			//Start Menu
	        $menu_list .= '			<ul id="nav-mobile" class="right">' ."\n";

	        //Proccess Menu
	        foreach( $menu_items as $menu_item ) 
	        {
		        //Check Just Parent Items
	            if( $menu_item->menu_item_parent == 0 ) 
	            {   
		            //Assign Parent ID & Create Menu Array
	                $parent = $menu_item->ID;
	                $menu_array = array();
	                
	                //Proccess Items Menu
	                foreach( $menu_items as $submenu ) 
	                {
		                //Check if Item is Parent
	                    if( $submenu->menu_item_parent == $parent ) 
	                    {
		                    //Flag Submenu
	                        $bool = true;
	                        
	                        //Build Submenu Item
	                        $menu_array[] = '				<li><a class="din font24 black-text" target="_blank" href="'.$submenu->url.'">'.$submenu->title.'</a></li>' ."\n";
	                    }
	                }
	                
	                //Check If Item has a Submenu
	                if( $bool == true && count( $menu_array ) > 0 ) 
	                {
		                //Build Menu Item
	                	$menu_list .= '				<li><a class="din font24 dropdown-button" data-activates="'.sanitize_title($menu_item->title).'-hover" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
	                    
	                    //Add Submenu Items
	                    $menu_list .= '<ul id="'.sanitize_title($menu_item->title).'-hover" class="dropdown-content">' ."\n";
	                    $menu_list .= implode( "\n", $menu_array );
	                    $menu_list .= '</ul>' ."\n";
	                     
	                } 
	                else 
	                {
		                //Build Menu Item without Submenu
	                    $menu_list .= '				<li><a class="din font24" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
	                }
	                 
	            }
	        }
	        
	        //Add Language Links
	        $menu_list .= '				<li class="din font24"><a class="din font24 inline no-padding" href="'.$es.'">ESP</a> / <a class="din font24 inline no-padding" href="'.$en.'">ENG</a></li>';     
	        
	        //Close Desktop Menu
	        $menu_list .= '			</ul>' ."\n";
	        
	        //Close Navbar
	        $menu_list .= '		</div>' ."\n";
	        $menu_list .= '	</nav>' ."\n";
	        $menu_list .= '</div>' ."\n";
	    } 
	    else 
	    {
		    //Menu does not exists
	        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
	    }
	     
	    echo $menu_list;
	}
	
	//Custom Site Menu
	function create_site_menu( $theme_location ) 
	{
		//Check Theme Location
	    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) 
	    {
		    //Check Languages
		    $languages = icl_get_languages('skip_missing=1'); 
		    $es = isset($languages['es']['url']) ? $languages['es']['url'] : '#'; 
		    $en = isset($languages['en']['url']) ? $languages['en']['url'] : '#';
		    
		    //Check Page
		    if (!is_page('site')) { $nav_url = get_bloginfo('url').'/site'; } else { $nav_url = ''; }
		    
		    //Buit Navbar
	        $menu_list  = '<div class="navbar-fixed">' ."\n";
	        $menu_list .= '	<nav class="black menu">' ."\n";
	        $menu_list .= '		<div class="nav-wrapper container-fluid">' ."\n";
	        
	        //Add Logo
	        $menu_list .= '			<a href="' . get_bloginfo('url').'" class="brand-logo">' . "\n";
	        $menu_list .= '				<img src="' . get_bloginfo("template_directory") . '/img/logo-site.png"/>' ."\n";
	        $menu_list .= '			</a>' ."\n";
	        
	        //Add Mobile Menu Button
	        $menu_list .= '			<a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>' ."\n";
	        
	        //Read Menu Items 
	        $menu = get_term( $locations[$theme_location], 'nav_menu' );
	        $menu_items = wp_get_nav_menu_items($menu->term_id);
	 
			//Start Menu
	        $menu_list .= '			<ul id="nav-mobile" class="left hide-on-med-and-down">' ."\n";
	        
	        //Proccess Menu
	        foreach( $menu_items as $menu_item ) 
	        {
		        //Check Just Parent Items
	            if( $menu_item->menu_item_parent == 0 ) 
	            {   
		            //Assign Parent ID & Create Menu Array
	                $parent = $menu_item->ID;
	                $menu_array = array();
	                
	                //Proccess Items Menu
	                foreach( $menu_items as $submenu ) 
	                {
		                //Check if Item is Parent
	                    if( $submenu->menu_item_parent == $parent ) 
	                    {
		                    //Flag Submenu
	                        $bool = true;
	                        
	                        //Build Submenu Item
	                        $menu_array[] = '				<li><a class="din font24 black-text" target="_blank" href="'.$nav_url.$submenu->url.'">'.$submenu->title.'</a></li>' ."\n";
	                    }
	                }
	                
	                //Check If Item has a Submenu
	                if( $bool == true && count( $menu_array ) > 0 ) 
	                {
		                //Build Menu Item
	                	$menu_list .= '				<li><a class="din font22 dropdown-button" data-activates="'.sanitize_title($menu_item->title).'-hover" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
	                    
	                    //Add Submenu Items
	                    $menu_list .= '<ul id="'.sanitize_title($menu_item->title).'-hover" class="dropdown-content">' ."\n";
	                    $menu_list .= implode( "\n", $menu_array );
	                    $menu_list .= '</ul>' ."\n";
	                     
	                } 
	                else 
	                {
		                if (strtolower($menu_item->title) != 'store')
		                {
			                //Build Menu Item without Submenu
		                    $menu_list .= '				<li><a class="din font22" href="'.$nav_url.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
		                }
		                else
		                {
			                //Check Kichink Store ID
					        if (get_field("kichink_site", "option"))
					        {
						    	//Build Menu Item without Submenu
								$menu_list .= '				<li><a class="din font22" href="'.$nav_url.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
						    }
		                }
	                }
	                 
	            }
	        }
	        
	        //End Menu
	        $menu_list .= '				</ul>';
	        
	        //Cart Menu
	        $menu_list .= '			<ul id="cart-mobile" class="right">' ."\n";
	        
	        //Check Kichink Store ID
	        if (get_field("kichink_site", "option"))
	        {
		        //Add Shopping Cart
		        //$menu_list .= '				<a href="#" class="din font22 inline btn-shopping"><svg id="shopping" data-name="shopping" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.6 45.5"><title>car</title><path d="M49.6,11.1H20.1a1.2,1.2,0,0,0-1.2,1.7l4.7,13.7a2,2,0,0,0,1.8,1.3H44.7a2,2,0,0,0,1.8-1.3l4.1-14A1,1,0,0,0,49.6,11.1Z" transform="translate(0 -2.6)"/><circle cx="22.7" cy="41" r="4.4"/><circle cx="41.4" cy="41.1" r="4.4"/><path d="M46.7,32.3H20.9L10.5,2.6h-8a2.5,2.5,0,1,0,0,5H7L17.3,37.3H46.7a2.5,2.5,0,1,0,0-5Z" transform="translate(0 -2.6)"/></svg> | 2</a>';
				$menu_list .= '				<span id="kichink-shoppingkart"></span>';
			}
				
	        //End Social Icons
	        $menu_list .= '					</li>';
	        
	        //Close Navbar
	        $menu_list .= '			</ul>' ."\n";
	        
	        //Start Social Icons
	        $menu_list .= '			<ul id="social-mobile" class="right hide-on-med-and-down">' ."\n";
	        
			$menu_list .= '					<li class="din font22 social">';
	        
	        //Add Facebook
	        if (get_field("facebook_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("facebook_link", "option").'" target="_blank" class="din font22 inline"><svg id="facebook" data-name="facebook" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.5 293.7"><title>fb</title><path d="M99,293.7V159.7h45l6.7-52.2H99V74.2c0-15.1,4.2-25.4,25.9-25.4h27.6V2.1A370,370,0,0,0,112.2,0C72.4,0,45.1,24.3,45.1,69v38.5H0v52.2H45.1V293.7Z"/></svg></a>';
	        }
	        
	        //Add Twitter
	        if (get_field("twitter_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("twitter_link", "option").'" target="_blank" class="din font22 inline"><svg id="twitter" data-name="twitter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202.3 164.4"><title>tw</title><path d="M202.3,19.5A83,83,0,0,1,178.4,26,41.6,41.6,0,0,0,196.7,3a83,83,0,0,1-26.3,10.1A41.5,41.5,0,0,0,99.6,50.9,117.8,117.8,0,0,1,14.1,7.6,41.5,41.5,0,0,0,26.9,63,41.3,41.3,0,0,1,8.1,57.8c0,.2,0,.3,0,.5A41.5,41.5,0,0,0,41.4,99a41.6,41.6,0,0,1-18.7.7,41.5,41.5,0,0,0,38.8,28.8A83.3,83.3,0,0,1,9.9,146.3a84.1,84.1,0,0,1-9.9-.6,117.4,117.4,0,0,0,63.6,18.6c76.3,0,118.1-63.2,118.1-118.1q0-2.7-.1-5.4A84.3,84.3,0,0,0,202.3,19.5Z"/></svg></a>';
	        }
	        
	        //Add Instagram
	        if (get_field("instagram_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("instagram_link", "option").'" target="_blank" class="din font22 inline"><svg id="instagram" data-name="instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.2 43.2"><title>in</title><path d="M14.4,21.6a7.2,7.2,0,1,1,7.2,7.2,7.2,7.2,0,0,1-7.2-7.2m-3.9,0A11.1,11.1,0,1,0,21.6,10.5,11.1,11.1,0,0,0,10.5,21.6m20-11.5a2.6,2.6,0,1,0,2.6-2.6,2.6,2.6,0,0,0-2.6,2.6M12.9,39.2a11.9,11.9,0,0,1-4-.7,6.7,6.7,0,0,1-2.5-1.6,6.7,6.7,0,0,1-1.6-2.5,12,12,0,0,1-.7-4c-.1-2.3-.1-3-.1-8.7s0-6.4.1-8.7a12,12,0,0,1,.7-4A6.7,6.7,0,0,1,6.4,6.4,6.7,6.7,0,0,1,8.9,4.8a11.9,11.9,0,0,1,4-.7H30.3a12,12,0,0,1,4,.7,6.7,6.7,0,0,1,2.5,1.6,6.7,6.7,0,0,1,1.6,2.5,12,12,0,0,1,.7,4c.1,2.3.1,3,.1,8.7s0,6.4-.1,8.7a12,12,0,0,1-.7,4,7.2,7.2,0,0,1-4.1,4.1,11.9,11.9,0,0,1-4,.7H12.9M12.7.1a15.8,15.8,0,0,0-5.2,1A10.6,10.6,0,0,0,3.6,3.6,10.6,10.6,0,0,0,1.1,7.4a15.8,15.8,0,0,0-1,5.2C0,15,0,15.7,0,21.6s0,6.6.1,8.9a15.8,15.8,0,0,0,1,5.2,10.6,10.6,0,0,0,2.5,3.8,10.6,10.6,0,0,0,3.8,2.5,15.8,15.8,0,0,0,5.2,1H30.5a15.8,15.8,0,0,0,5.2-1,11,11,0,0,0,6.3-6.3,15.8,15.8,0,0,0,1-5.2c.1-2.3.1-3,.1-8.9s0-6.6-.1-8.9a15.8,15.8,0,0,0-1-5.2,10.6,10.6,0,0,0-2.5-3.8,10.6,10.6,0,0,0-3.8-2.5,15.8,15.8,0,0,0-5.2-1H12.7"/></svg></a>';
	        }
	        
	        //Add Youtube
	        if (get_field("youtube_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("youtube_link", "option").'" target="_blank" class="din font22 inline"><svg id="youtube" data-name="youtube" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1021.4 718.4"><title>yt</title><path id="The_Sharpness" data-name="The Sharpness" class="cls-1" d="M407,206,649,367.6,683,350Z" transform="translate(-1.8 -1.3)"/><g id="Lozenge"><path d="M1013,156.3s-10-70.4-40.6-101.4C933.6,14.2,890,14,870.1,11.6,727.1,1.3,512.7,1.3,512.7,1.3h-.4s-214.4,0-357.4,10.3C135,14,91.4,14.2,52.6,54.9,22,85.9,12,156.3,12,156.3S1.8,238.9,1.8,321.6v77.5C1.8,481.8,12,564.4,12,564.4s10,70.4,40.6,101.4c38.9,40.7,89.9,39.4,112.6,43.7,81.7,7.8,347.3,10.3,347.3,10.3s214.6-.3,357.6-10.7c20-2.4,63.5-2.6,102.3-43.3,30.6-31,40.6-101.4,40.6-101.4s10.2-82.7,10.2-165.3V321.6C1023.2,238.9,1013,156.3,1013,156.3ZM407,493V206L683,350Z" transform="translate(-1.8 -1.3)"/></g></svg></a>';
	        }
	        
	        //Add Spotify
	        if (get_field("spotify_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("spotify_link", "option").'" target="_blank" class="din font22 inline"><svg id="spotify" data-name="spotify" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 508.5 508.5"><title>sp</title><path d="M254.3,0C113.8,0,0,113.8,0,254.3S113.8,508.5,254.3,508.5,508.5,394.7,508.5,254.3,394.7,0,254.3,0ZM371.7,403.3a17.3,17.3,0,0,1-15.3,9.1,17.5,17.5,0,0,1-8.4-2.1,278.2,278.2,0,0,0-215.8-21.9,17.4,17.4,0,0,1-21.8-11.5c-2.9-9.2,2.3-28.7,11.5-31.6a313.2,313.2,0,0,1,243,24.7C373.3,374.5,376.4,394.9,371.7,403.3ZM404,307.5c-3.6,7-10.7,18.3-18.1,18.3a19.4,19.4,0,0,1-9.4-2.3,353.1,353.1,0,0,0-255.2-27,20.4,20.4,0,0,1-25-14.3c-2.9-10.9,3.5-29.4,14.3-32.4a393.7,393.7,0,0,1,284.6,30.3A20.3,20.3,0,0,1,404,307.5Zm13.5-76.7a23.4,23.4,0,0,1-10.4-2.4,428.2,428.2,0,0,0-192.8-45.2,436.3,436.3,0,0,0-104.5,12.7,23.3,23.3,0,0,1-28.2-17c-3.1-12.5,4.5-27.6,17-30.6a480.2,480.2,0,0,1,115.7-14.1A474.8,474.8,0,0,1,428,184.2a23.3,23.3,0,0,1,10.5,31.2C434.3,223.7,426.1,230.8,417.5,230.8Z"/></svg></a>';
	        }
	        
	        //Add iTunes
	        if (get_field("itunes_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("itunes_link", "option").'" target="_blank" class="din font22 inline"><svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg></a>';
	        }
	        
	        //Add Mailing List
	        $menu_list .= '				<a href="#" class="din font22 inline btn-mailing"><svg id="mailing" data-name="mailing" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.9 19.5"><title>mailing</title><path class="cls-1" d="M2.2,1.2l10,7.9a2.6,2.6,0,0,0,1.7.5,2.5,2.5,0,0,0,1.7-.5l10-7.9c.8-.6.6-1.2-.4-1.2H2.6C1.6,0,1.4.5,2.2,1.2Z"/><path class="cls-1" d="M26.4,3.2l-11,8.3a2.7,2.7,0,0,1-3,0L1.5,3.2C.7,2.6,0,3,0,4V17.6a1.9,1.9,0,0,0,1.9,1.9H26a1.9,1.9,0,0,0,1.9-1.9V4C27.9,3,27.2,2.6,26.4,3.2Z"/></svg></a>';
	        
	        //Add Language Links
	        $menu_list .= '				<li class="din font22"><a class="din font22 inline no-padding" href="'.$es.'">ESP</a> / <a class="din font22 inline no-padding" href="'.$en.'">ENG</a></li>';
	        
	        //Close Navbar
	        $menu_list .= '			</ul>' ."\n";
	        
	        //Mobile Menu
	        $menu_list .= '			<ul class="side-nav" id="mobile-menu">';
			$menu_list .= '				<li class="din font22 social">';
			//Add Facebook
	        if (get_field("facebook_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("facebook_link", "option").'" target="_blank" class="din font22 inline"><svg id="facebook" data-name="facebook" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.5 293.7"><title>fb</title><path d="M99,293.7V159.7h45l6.7-52.2H99V74.2c0-15.1,4.2-25.4,25.9-25.4h27.6V2.1A370,370,0,0,0,112.2,0C72.4,0,45.1,24.3,45.1,69v38.5H0v52.2H45.1V293.7Z"/></svg></a>';
	        }
	        //Add Twitter
	        if (get_field("twitter_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("twitter_link", "option").'" target="_blank" class="din font22 inline"><svg id="twitter" data-name="twitter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202.3 164.4"><title>tw</title><path d="M202.3,19.5A83,83,0,0,1,178.4,26,41.6,41.6,0,0,0,196.7,3a83,83,0,0,1-26.3,10.1A41.5,41.5,0,0,0,99.6,50.9,117.8,117.8,0,0,1,14.1,7.6,41.5,41.5,0,0,0,26.9,63,41.3,41.3,0,0,1,8.1,57.8c0,.2,0,.3,0,.5A41.5,41.5,0,0,0,41.4,99a41.6,41.6,0,0,1-18.7.7,41.5,41.5,0,0,0,38.8,28.8A83.3,83.3,0,0,1,9.9,146.3a84.1,84.1,0,0,1-9.9-.6,117.4,117.4,0,0,0,63.6,18.6c76.3,0,118.1-63.2,118.1-118.1q0-2.7-.1-5.4A84.3,84.3,0,0,0,202.3,19.5Z"/></svg></a>';
	        }
	        
	        //Add Instagram
	        if (get_field("instagram_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("instagram_link", "option").'" target="_blank" class="din font22 inline"><svg id="instagram" data-name="instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.2 43.2"><title>in</title><path d="M14.4,21.6a7.2,7.2,0,1,1,7.2,7.2,7.2,7.2,0,0,1-7.2-7.2m-3.9,0A11.1,11.1,0,1,0,21.6,10.5,11.1,11.1,0,0,0,10.5,21.6m20-11.5a2.6,2.6,0,1,0,2.6-2.6,2.6,2.6,0,0,0-2.6,2.6M12.9,39.2a11.9,11.9,0,0,1-4-.7,6.7,6.7,0,0,1-2.5-1.6,6.7,6.7,0,0,1-1.6-2.5,12,12,0,0,1-.7-4c-.1-2.3-.1-3-.1-8.7s0-6.4.1-8.7a12,12,0,0,1,.7-4A6.7,6.7,0,0,1,6.4,6.4,6.7,6.7,0,0,1,8.9,4.8a11.9,11.9,0,0,1,4-.7H30.3a12,12,0,0,1,4,.7,6.7,6.7,0,0,1,2.5,1.6,6.7,6.7,0,0,1,1.6,2.5,12,12,0,0,1,.7,4c.1,2.3.1,3,.1,8.7s0,6.4-.1,8.7a12,12,0,0,1-.7,4,7.2,7.2,0,0,1-4.1,4.1,11.9,11.9,0,0,1-4,.7H12.9M12.7.1a15.8,15.8,0,0,0-5.2,1A10.6,10.6,0,0,0,3.6,3.6,10.6,10.6,0,0,0,1.1,7.4a15.8,15.8,0,0,0-1,5.2C0,15,0,15.7,0,21.6s0,6.6.1,8.9a15.8,15.8,0,0,0,1,5.2,10.6,10.6,0,0,0,2.5,3.8,10.6,10.6,0,0,0,3.8,2.5,15.8,15.8,0,0,0,5.2,1H30.5a15.8,15.8,0,0,0,5.2-1,11,11,0,0,0,6.3-6.3,15.8,15.8,0,0,0,1-5.2c.1-2.3.1-3,.1-8.9s0-6.6-.1-8.9a15.8,15.8,0,0,0-1-5.2,10.6,10.6,0,0,0-2.5-3.8,10.6,10.6,0,0,0-3.8-2.5,15.8,15.8,0,0,0-5.2-1H12.7"/></svg></a>';
	        }
	        
	        //Add Youtube
	        if (get_field("youtube_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("youtube_link", "option").'" target="_blank" class="din font22 inline"><svg id="youtube" data-name="youtube" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1021.4 718.4"><title>yt</title><path id="The_Sharpness" data-name="The Sharpness" class="cls-1" d="M407,206,649,367.6,683,350Z" transform="translate(-1.8 -1.3)"/><g id="Lozenge"><path d="M1013,156.3s-10-70.4-40.6-101.4C933.6,14.2,890,14,870.1,11.6,727.1,1.3,512.7,1.3,512.7,1.3h-.4s-214.4,0-357.4,10.3C135,14,91.4,14.2,52.6,54.9,22,85.9,12,156.3,12,156.3S1.8,238.9,1.8,321.6v77.5C1.8,481.8,12,564.4,12,564.4s10,70.4,40.6,101.4c38.9,40.7,89.9,39.4,112.6,43.7,81.7,7.8,347.3,10.3,347.3,10.3s214.6-.3,357.6-10.7c20-2.4,63.5-2.6,102.3-43.3,30.6-31,40.6-101.4,40.6-101.4s10.2-82.7,10.2-165.3V321.6C1023.2,238.9,1013,156.3,1013,156.3ZM407,493V206L683,350Z" transform="translate(-1.8 -1.3)"/></g></svg></a>';
	        }
	        
	        //Add Spotify
	        if (get_field("spotify_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("spotify_link", "option").'" target="_blank" class="din font22 inline"><svg id="spotify" data-name="spotify" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 508.5 508.5"><title>sp</title><path d="M254.3,0C113.8,0,0,113.8,0,254.3S113.8,508.5,254.3,508.5,508.5,394.7,508.5,254.3,394.7,0,254.3,0ZM371.7,403.3a17.3,17.3,0,0,1-15.3,9.1,17.5,17.5,0,0,1-8.4-2.1,278.2,278.2,0,0,0-215.8-21.9,17.4,17.4,0,0,1-21.8-11.5c-2.9-9.2,2.3-28.7,11.5-31.6a313.2,313.2,0,0,1,243,24.7C373.3,374.5,376.4,394.9,371.7,403.3ZM404,307.5c-3.6,7-10.7,18.3-18.1,18.3a19.4,19.4,0,0,1-9.4-2.3,353.1,353.1,0,0,0-255.2-27,20.4,20.4,0,0,1-25-14.3c-2.9-10.9,3.5-29.4,14.3-32.4a393.7,393.7,0,0,1,284.6,30.3A20.3,20.3,0,0,1,404,307.5Zm13.5-76.7a23.4,23.4,0,0,1-10.4-2.4,428.2,428.2,0,0,0-192.8-45.2,436.3,436.3,0,0,0-104.5,12.7,23.3,23.3,0,0,1-28.2-17c-3.1-12.5,4.5-27.6,17-30.6a480.2,480.2,0,0,1,115.7-14.1A474.8,474.8,0,0,1,428,184.2a23.3,23.3,0,0,1,10.5,31.2C434.3,223.7,426.1,230.8,417.5,230.8Z"/></svg></a>';
	        }
	        
	        //Add iTunes
	        if (get_field("itunes_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("itunes_link", "option").'" target="_blank" class="din font22 inline"><svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg></a>';
	        }
	        
	        $menu_list .= '				</li>';
			
			//Menu Items
			foreach( $menu_items as $menu_item ) 
			{
				$menu_list .= '				<li><a class="din font22 black-text centered" href="'.$nav_url.$menu_item->url.'">'.$menu_item->title.'</a></li>';
			}
			
			//Add Language Links
	        $menu_list .= '				<li class="din font22 black-text centered"><a class="din font22 inline no-padding" href="'.$es.'">ESP</a> / <a class="din font22 inline no-padding" href="'.$en.'">ENG</a></li>';     
			
			//Separator
	        $menu_list .= '				<li><hr /></li>';
	        
	        //Mailing List
	        $menu_list .= '				<li class="din font22 black-text centered">'.__("JOIN MAILING LIST", "reypila_v1").'</li>';
			
			//Mailchimp Form Embeed
			$menu_list .= '				<li rel="form">';
			$menu_list .= '					<div id="mc_embed_signup">';
			$menu_list .= '						<form action="//reypila.us14.list-manage.com/subscribe/post-json?u=a68dc2f44e7fd847725e74dc2&amp;id=17421f7c4e&amp;c=?" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>';
			$menu_list .= '							<div id="mc_embed_signup_scroll">';
			$menu_list .= '								<div class="mc-field-group">';
			$menu_list .= '									<input type="text" value="" placeholder="'.__("Name","reypila_v1").'" name="FNAME" class="din font22 black-text" id="mce-FNAME">';
			$menu_list .= '								</div>';
			$menu_list .= '								<div class="mc-field-group">';
			$menu_list .= '									<input type="text" value="" placeholder="'.__("Lastname","reypila_v1").'" name="LNAME" class="din black-text font22" id="mce-LNAME">';
			$menu_list .= '								</div>';
			$menu_list .= '								<div class="mc-field-group">';
			$menu_list .= '									<input type="email" value="" placeholder="'.__("Email","reypila_v1").'" name="EMAIL" class="required email din black-text font22" id="mce-EMAIL">';
			$menu_list .= '								</div>';
			$menu_list .= '								<div class="mc-field-group">';
			$menu_list .= '									<input type="text" value="" placeholder="'.__("Country","reypila_v1").'" name="MMERGE5" class="din black-text font22" id="mce-MMERGE5">';
			$menu_list .= '								</div>';
			$menu_list .= '								<div id="mce-responses" class="clear">';
			$menu_list .= '									<div class="response" id="mce-error-response" style="display:none"></div>';
			$menu_list .= '									<div class="response" id="mce-success-response" style="display:none"></div>';
			$menu_list .= '								</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->';
			$menu_list .= '								<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a68dc2f44e7fd847725e74dc2_17421f7c4e" tabindex="-1" value=""></div>';
			$menu_list .= '								<div class="clear"><input type="submit" value="'.__("Submit","reypila_v1").'" name="subscribe" id="mc-embedded-subscribe" class="button btn-flat btn-block black-text din font22"></div>';
			$menu_list .= '							</div>';
			$menu_list .= '						</form>';
			$menu_list .= '					</div>';
			$menu_list .= '				</li>';
			
			$menu_list .= '			</ul>';
			
	        $menu_list .= '		</div>' ."\n";
	        $menu_list .= '	</nav>' ."\n";
	        $menu_list .= '</div>' ."\n";
	    } 
	    else 
	    {
		    //Menu does not exists
	        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
	    }
	     
	    echo $menu_list;
	}
	
	add_filter('next_posts_link_attributes', 'posts_link_attributes');
	add_filter('previous_posts_link_attributes', 'posts_link_attributes');
	
	function posts_link_attributes() {
	    return 'class="waves-effect waves-light btn-flat btn-site-b helvetica font14"';
	}

	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) {
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Rey Pila</i>';
	}
	function my_footer_version() {
	    return 'Version 1.0';
	}

	// Páginas de Configuración
	add_filter('acf/options_page/settings', 'my_options_page_settings');

	function my_options_page_settings ( $options )
	{
		$options['title'] = __('Configuración');
		$options['pages'] = array(
			__('Landing'),
			__('Site'),
			__('Footer')
		);

		return $options;
	}

	/* Definición de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );

	/* Enqueue scripts and styles. */
	function scripts() {
		// Load CSS
		wp_enqueue_style( 'fontMaterialDesign', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		wp_enqueue_style( 'kichink', 'https://www.kichink.com/v2/themes/css/shoppingcart.css' );
		wp_enqueue_style( 'styles', CSSPATH . 'app.css', array(), '1.0.11' );
		// Load JS
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', JSPATH . 'app.js', array(), '1.0.3', true );
		wp_enqueue_script('kichink', 'https://www.kichink.com/v2/themes/js/shoppingkart.js', array('jquery'), '1.0.6', true );
	}
	add_action( 'wp_enqueue_scripts', 'scripts' );

	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	function codex_custom_init() {

		//News
		$labels = array(
		    'name' => _x('News', 'post type general name'),
		    'singular_name' => _x('New', 'post type singular name'),
		    'add_new' => _x('Add New Item', 'New'),
		    'add_new_item' => __('Add New Item'),
		    'edit_item' => __('Edit New'),
		    'new_item' => __('New Item'),
		    'all_items' => __('All Items'),
		    'view_item' => __('View Item'),
		    'search_items' => __('Search Items'),
		    'not_found' =>  __('Not Found'),
		    'not_found_in_trash' => __('Not Found In Trash'),
		    'parent_item_colon' => '',
		    'menu_name' => 'News'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-megaphone',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		);
		register_post_type('new',$args);
		
		//Products
		$labels = array(
		    'name' => _x('Products', 'post type general name'),
		    'singular_name' => _x('Product', 'post type singular name'),
		    'add_new' => _x('Add New', 'New'),
		    'add_new_item' => __('Add New Product'),
		    'edit_item' => __('Edit Product'),
		    'new_item' => __('New Product'),
		    'all_items' => __('All Products'),
		    'view_item' => __('View Product'),
		    'search_items' => __('Search Products'),
		    'not_found' =>  __('Not Found'),
		    'not_found_in_trash' => __('Not Found In Trash'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Products'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-cart',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		);
		register_post_type('product',$args);
	}

	//Funcion para identar JSON
	function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo indent($json);
	}
	
	//Funcion para obtener el RGBA
	function hex2rgba($color, $opacity = false) 
	{			 
		$default = 'rgb(0,0,0)';
	 
		//Return default if no color provided
		if(empty($color)) return $default; 
	 
		//Sanitize $color if "#" is provided 
		if ($color[0] == '#' ) 
		{
			$color = substr( $color, 1 );
		}
 
		//Check if color has 6 or 3 characters and get values
		if (strlen($color) == 6) 
		{
			$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} 
		elseif ( strlen( $color ) == 3 ) 
		{
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} 
		else 
		{
			return $default;
		}
 
		//Convert hexadec to rgb
		$rgb =  array_map('hexdec', $hex);
 
		//Check if opacity is set(rgba or rgb)
		if($opacity)
		{
			if(abs($opacity) > 1)
				$opacity = 1.0;
				$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
		} 
		else 
		{
			$output = 'rgb('.implode(",",$rgb).')';
		}
 
		//Return rgb(a) color string
		return $output;
	}

	//SUBIR IMAGEN A CAMPO IMG DE ACF
	function my_update_attachment($f,$pid,$t='',$c='') {
	  	wp_update_attachment_metadata( $pid, $f );
	  	if( !empty( $_FILES[$f]['name'] )) { //New upload
	    	require_once( ABSPATH . 'wp-admin/includes/file.php' );
			include( ABSPATH . 'wp-admin/includes/image.php' );
			// $override['action'] = 'editpost';
			$override['test_form'] = false;
			$file = wp_handle_upload( $_FILES[$f], $override );

			if ( isset( $file['error'] )) {
				return new WP_Error( 'upload_error', $file['error'] );
	    	}

			$file_type = wp_check_filetype($_FILES[$f]['name'], array(
				'jpg|jpeg' => 'image/jpeg',
				'gif' => 'image/gif',
				'png' => 'image/png',
			));

			if ($file_type['type']) {
				$name_parts = pathinfo( $file['file'] );
				$name = $file['filename'];
				$type = $file['type'];
				$title = $t ? $t : $name;
				$content = $c;

				$attachment = array(
					'post_title' => $title,
					'post_type' => 'attachment',
					'post_content' => $content,
					'post_parent' => $pid,
					'post_mime_type' => $type,
					'guid' => $file['url'],
				);

				foreach( get_intermediate_image_sizes() as $s ) {
					$sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
					$sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
					$sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
					$sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
	      		}

		  		$sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

		  		foreach( $sizes as $size => $size_data ) {
		  			$resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
		  			if ( $resized )
		  				$metadata['sizes'][$size] = $resized;
	      		}

		  		$attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

		  		if ( !is_wp_error( $attach_id )) {
		  			$attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
		  			wp_update_attachment_metadata( $attach_id, $attach_meta );
	      		}

		  		return array(
		  			'pid' =>$pid,
		  			'url' =>$file['url'],
		  			'file'=>$file,
		  			'attach_id'=>$attach_id
		  		);
	    	}
	  	}
	}
	

?>