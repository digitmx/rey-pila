<?php get_header(); ?>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); $exclude_ids = array( $post->ID ); ?>

		<!-- NAVBAR -->
		<?php get_template_part("includes/navbar"); ?>
		
		<div class="container" id="store">
	    	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<span class="din font48 black-text centered block"><?php _e("STORE","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
	      	
	      	<div class="row">
		      	<div class="col s12 m6 l8">
			      	<?php $gallery = get_field("gallery", $post->ID); ?>
			      	<div class="slider">
				      	<ul class="slides">
					    	<?php foreach ($gallery as $item) { ?>
							<li>
								<img class="responsive-img" src="<?php echo $item['url']; ?>">
							</li> 
							<?php } ?>
					    </ul>			      	
					</div>
		      	</div>
		      	<div class="col s12 m6 l4">
			      	<div class="space40 hide-on-small-only"></div>
			      	<span class="din font48 block"><?php the_field("price", $post->ID); ?></span>
			      	<span class="din font30 block magnesium-text"><?php the_title(); ?></span>
			      	<div class="space20"></div>
			      	<div class="space20 hide-on-small-only"></div>
			      	<a href="#" id="kichink-buybutton" data-id="<?php the_field("product_id", $post->ID); ?>" class="waves-effect waves-light btn-flat btn-site-w helvetica font14"><?php _e("BUY","reypila_v1"); ?></a>
		      	</div>
	      	</div>
	      	
		</div>
		
		<?php endwhile; ?>
		<?php endif; ?>
		
		<?php 
		    //Query Products
		    $args = array(
				'posts_per_page'   => 6,
				'order'			   => 'date',
				'orderby'          => 'DESC',
				'post_type'        => 'product',
				'post_status'      => 'publish',
				'post__not_in'	   => $exclude_ids,
				'suppress_filters' => true 
			);
			$products = get_posts( $args ); 
		?>
		<div class="container">
			<div class="row">
				<div class="space20"></div>
				<?php foreach ($products as $product) { //Proccess Products ?>
				<div class="col s12 m6 l4">
					<a href="<?php echo get_permalink($product->ID); ?>">
						<img class="responsive-img block" src="<?php echo get_the_post_thumbnail_url($product->ID); ?>" />
						<div class="space10"></div>
						<span class="din font24 black-text block"><?php the_field("price", $product->ID); ?></span>
						<span class="din font16 magnesium-text block"><?php echo $product->post_title; ?></span>
						<div class="space20"></div>
					</a>
				</div>
				<?php } ?>
			</div> 	
		</div>
		
		<hr />
		
		<!-- CONTACTS -->
		<?php get_template_part("includes/contacts"); ?>
		
		
<?php get_footer(); ?>