<?php /* Template Name: Store */ ?>

<?php get_header(); ?>

		<!-- NAVBAR -->
		<?php get_template_part("includes/navbar"); ?>
		
		<!-- STORE -->
      	<?php if (get_field("kichink_site", "option")) { ?>
      	<div class="container" id="store">
	    	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<span class="din font48 black-text centered block"><?php _e("STORE","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
	      	
	      	<?php 
			    //Query Products
			    $args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'product',
					'post_status'      => 'publish',
					
				);
				$products = get_posts( $args ); 
			?>
			<div class="row">
				<?php foreach ($products as $product) { //Proccess Products ?>
				<div class="col s12 m6 l4">
					<a href="<?php echo get_permalink($product->ID); ?>">
						<img class="responsive-img block" src="<?php echo get_the_post_thumbnail_url($product->ID); ?>" />
						<div class="space10"></div>
						<span class="din font24 black-text block"><?php the_field("price", $product->ID); ?></span>
						<span class="din font16 magnesium-text block"><?php echo $product->post_title; ?></span>
						<div class="space20"></div>
					</a>
				</div>
				<?php } ?>
			</div>
		    
      	</div>
      	<?php } else { ?>
      	<div class="container" id="store">
	    	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<span class="din font48 black-text centered block"><?php _e("STORE","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
		  	<div class="row">
			  	<div class="col s12 m12 l12">
				  	<div class="space20"></div>
				  	<span class="din font24 black-text block"><?php _e("There are no products to show.","reypila_v1"); ?></span>
				  	<div class="space20"></div>
				</div>
	      	</div>
      	</div>
      	<?php } ?>
      	
      	<hr />
		
		<!-- CONTACTS -->
		<?php get_template_part("includes/contacts"); ?>
		
<?php get_footer(); ?>