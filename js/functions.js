
	$( document ).ready(function(){
		
		setTimeout(function() {
		    $('.se-pre-con').fadeOut('slow');
		}, 2000);
		
		//Read Values 
		var base_url = $('#base_url').val();
		var kichink = $('#kichink').val();
		
		//Kichink Config
		$('#kichink-shoppingkart').ShoppingKart({
            text: 'REY PILA',
            beforeText: 'I WANT IT',
			store_id: kichink,
			theme: 'button',
			button: '#kichink-buybutton',
			placement: 'left',
			showOnPurchase: true,
			checkoutURI: 'https://www.kichink.com/checkout'
		});
		
		//Mobile Button
		$(".button-collapse").sideNav();
		
		//Carrousl
		$('.carousel.carousel-slider').carousel({fullWidth: true});
		
		//Close Mobile Nav
		$("#mobile-menu li").click(function() { 
			//Read Rel Form
			var rel = $(this).attr('rel');
			
			//Check Rel
			if (!rel)
			{
				//Close Sidebar
				$('.button-collapse').sideNav('hide'); 
			}
		});
		
		//Dropdown Setup
		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrainWidth: false, // Does not change width of dropdown to that of the activator
			hover: true, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'left', // Displays dropdown with edge aligned to the left of button
			stopPropagation: false // Stops event propagation
		
    	});
    	
    	//Slider Site
    	$('.slider').slider({
	    	indicators: true
    	});
    	
    	//Cover Live Site
    	$('.parallax').parallax();
    	
    	//Slick
    	$('#div_albums').slick({
		    infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true,
		});
		
		//Slick
    	$('#div_singles').slick({
		    infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true,
		});
		
		//Slick
    	$('#div_videos').slick({
		    infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true,
		});
    	
    	//btnShareTwitter Click
		$('.btnShareTwitter').on('click', function(e) {
			e.preventDefault();
			
			var url = $(this).attr('rel');
			var text = $(this).attr('text');
	        var width  = 575,
	            height = 400,
	            left   = ($(window).width()  - width)  / 2,
	            top    = ($(window).height() - height) / 2,
	            url    = 'http://twitter.com/share?url=' + url + '&text=' + text,
	            opts   = 'status=1' +
	                     ',width='  + width  +
	                     ',height=' + height +
	                     ',top='    + top    +
	                     ',left='   + left;

	        window.open(url, 'twitter', opts);

	        return false;
	    });

		//btnShareFacebook Click
	    $('.btnShareFacebook').on('click', function(e) {
		    e.preventDefault();
			
			var url = $(this).attr('rel');
	        var width  = 575,
	            height = 400,
	            left   = ($(window).width()  - width)  / 2,
	            top    = ($(window).height() - height) / 2,
	            url    = 'http://www.facebook.com/sharer.php?s=100&p[url]=' + url,
	            opts   = 'status=1' +
	                     ',width='  + width  +
	                     ',height=' + height +
	                     ',top='    + top    +
	                     ',left='   + left;

	        window.open(url, 'facebook', opts);

	        return false;
	    });
	    
	    //tab menu click
	    $('.tabs .tab a').on('click', function(e) {
		    
			var rel = $(this).attr('rel');
			$('#div_' + rel).slick('slickGoTo',0);
			console.log(rel);
		   	 
	    });
	    
	    /*
	    //btn section music Click
	    $('.site #music #menu a').on('click', function(e) {
		    e.preventDefault();
		   	
		   	//Read Values
			var section = $(this).attr('rel');
			var background = $(this).attr('background');
			
			//Hide All Sections
		   	$('.site #music .parallax-container #sections .section').fadeOut();
		   	
		   	//Show Section
		   	$('.site #music .parallax-container #sections #' + section).fadeIn();
		   	
		   	//Change Menu Selected
		   	$('.site #music .parallax-container #menu div a').removeClass('white-text');
		   	$('.site #music .parallax-container #menu div a').removeClass('underline');
		   	$('.site #music .parallax-container #menu div a').removeClass('bold');
		   	$('.site #music .parallax-container #menu div a').addClass('magnesium-text');
		   	$(this).removeClass('magnesium-text');
		   	$(this).addClass('white-text');
		   	$(this).addClass('underline');
		   	$(this).addClass('bold');
		   	
		   	//Change Background
		   	$('.site #music .parallax-container .parallax img').attr('src', background);
		   	$('.parallax').parallax();
		   	 
		    return false;
	    });
	    
	    //btn section album Click
	    $('.site #music #sections #albums #text a').on('click', function(e) {
		    e.preventDefault();
		   	
		   	//Read Values
			var section = $(this).attr('rel');
			var background = $(this).attr('background');
		   	
		   	//Hide All Sections
		   	$('.site #music .parallax-container #sections .section').fadeOut();
		   	
		   	//Show Section
		   	$('.site #music .parallax-container #sections #' + section).fadeIn();
		   	
		   	//Change Background
		   	$('.site #music .parallax-container .parallax img').attr('src', background);
		   	$('.parallax').parallax();
		   	 
		    return false;
	    });
	    
	    //btn section album Click
	    $('.site #music #sections #singles #text a').on('click', function(e) {
		    e.preventDefault();
		   	
		   	//Read Values
			var section = $(this).attr('rel');
			var background = $(this).attr('background');
		   	
		   	//Hide All Sections
		   	$('.site #music .parallax-container #sections .section').fadeOut();
		   	
		   	//Show Section
		   	$('.site #music .parallax-container #sections #' + section).fadeIn();
		   	
		   	//Change Background
		   	$('.site #music .parallax-container .parallax img').attr('src', background);
		   	$('.parallax').parallax();
		   	 
		    return false;
	    });
	    */
	    
	    //Modal Setup
		$('.modal').modal({
			dismissible: true, // Modal can be dismissed by clicking outside of the modal
			opacity: 1.0, // Opacity of modal background
			in_duration: 300, // Transition in duration
			out_duration: 200, // Transition out duration
			starting_top: '0%', // Starting top style attribute
			ending_top: '0%', // Ending top style attribute
			ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
				$('nav').fadeOut('slow');
      		},
	  		complete: function() { $('nav').fadeIn(); $(this).find('iframe').attr('src', $(this).find('iframe').attr('src')); } // Callback for Modal close
		});
		
		//Modal Close
		$('.close-modal').on('click', function(e) {
			e.preventDefault();
			
			$('.modal').modal('close');
			
			return false;
		});
		
		//Btn Mailing Click
		$('.btn-mailing').on('click', function(e) {
			e.preventDefault();
			
			//Read Form
			var $form = $('#nav-mailing #mc-embedded-subscribe-form');
			
			//Remove Errors
			$('#nav-mailing #mc_embed_signup #mce-FNAME').removeClass('input-error');
			$('#nav-mailing #mc_embed_signup #mce-LNAME').removeClass('input-error');
			$('#nav-mailing #mc_embed_signup #mce-EMAIL').removeClass('input-error');
			$('#nav-mailing #mc_embed_signup #mce-MMERGE5').removeClass('input-error');
			
			//Reset Form
			$form[0].reset();
			
			//Show Mailing Bar
			$('#nav-mailing').toggle();
			
			return false;
		});
		
		//Subscribe Navbar Menu
		$('#nav-mailing #mc_embed_signup #mc-embedded-subscribe').on('click', function(e) {
			e.preventDefault();
			
			//Remove Errors
			$('#nav-mailing #mc_embed_signup #mce-FNAME').removeClass('input-error');
			$('#nav-mailing #mc_embed_signup #mce-LNAME').removeClass('input-error');
			$('#nav-mailing #mc_embed_signup #mce-EMAIL').removeClass('input-error');
			$('#nav-mailing #mc_embed_signup #mce-MMERGE5').removeClass('input-error');
			
			//Read Values
			var name = $('#nav-mailing #mc_embed_signup #mce-FNAME').val().trim();
			var lastname = $('#nav-mailing #mc_embed_signup #mce-LNAME').val().trim();
			var email = $('#nav-mailing #mc_embed_signup #mce-EMAIL').val().trim();
			var country = $('#nav-mailing #mc_embed_signup #mce-MMERGE5').val().trim();
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			
			//Check Values
			if (name && lastname && email && country)
			{
				//Check Email
				if (regex.test(email))
				{
					var $form = $('#nav-mailing #mc-embedded-subscribe-form');
					var $msg = $('#msg_thanks').val();
					
					$.ajax({
				        type: $form.attr('method'),
				        url: $form.attr('action'),
				        data: $form.serialize(),
				        cache       : false,
				        dataType    : 'json',
				        contentType: "application/json; charset=utf-8",
				        error       : function(err) { Materialize.toast('Error Server', 4000); },
				        success     : function(data) {
				            if (data.result != "success") {
					            Materialize.toast(data.msg, 4000);
				            } else {
				                Materialize.toast($msg, 4000);
				                $form[0].reset();
				            }
				        }
				    });
				}
				else
				{
					//Send Error for Invalid Email
					$('#nav-mailing #mc_embed_signup #mce-EMAIL').addClass('input-error');
				}
			}
			else
			{
				//Check Errors
				if (!name) { $('#nav-mailing #mc_embed_signup #mce-FNAME').addClass('input-error'); }
				if (!lastname) { $('#nav-mailing #mc_embed_signup #mce-LNAME').addClass('input-error'); }
				if (!email) { $('#nav-mailing #mc_embed_signup #mce-EMAIL').addClass('input-error'); }
				if (!country) { $('#nav-mailing #mc_embed_signup #mce-MMERGE5').addClass('input-error'); }
			}
			
			return false;
		});
		
		//Subscribe Mobile Menu
		$('#mobile-menu #mc_embed_signup #mc-embedded-subscribe').on('click', function(e) {
			e.preventDefault();
			
			//Remove Errors
			$('#mobile-menu #mc_embed_signup #mce-FNAME').removeClass('input-error');
			$('#mobile-menu #mc_embed_signup #mce-LNAME').removeClass('input-error');
			$('#mobile-menu #mc_embed_signup #mce-EMAIL').removeClass('input-error');
			$('#mobile-menu #mc_embed_signup #mce-MMERGE5').removeClass('input-error');
			
			//Read Values
			var name = $('#mobile-menu #mc_embed_signup #mce-FNAME').val().trim();
			var lastname = $('#mobile-menu #mc_embed_signup #mce-LNAME').val().trim();
			var email = $('#mobile-menu #mc_embed_signup #mce-EMAIL').val().trim();
			var country = $('#mobile-menu #mc_embed_signup #mce-MMERGE5').val().trim();
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			
			//Check Values
			if (name && lastname && email && country)
			{
				//Check Email
				if (regex.test(email))
				{
					var $form = $('#mobile-menu #mc-embedded-subscribe-form');
					var $msg = $('#msg_thanks').val();
					
					$.ajax({
				        type: $form.attr('method'),
				        url: $form.attr('action'),
				        data: $form.serialize(),
				        cache       : false,
				        dataType    : 'json',
				        contentType: "application/json; charset=utf-8",
				        error       : function(err) { Materialize.toast('Error Server', 4000); },
				        success     : function(data) {
				            if (data.result != "success") {
					            Materialize.toast(data.msg, 4000);
				            } else {
				                Materialize.toast($msg, 4000);
				                $form[0].reset();
				            }
				        }
				    });
				}
				else
				{
					//Send Error for Invalid Email
					$('#mobile-menu #mc_embed_signup #mce-EMAIL').addClass('input-error');
				}
			}
			else
			{
				//Check Errors
				if (!name) { $('#mobile-menu #mc_embed_signup #mce-FNAME').addClass('input-error'); }
				if (!lastname) { $('#mobile-menu #mc_embed_signup #mce-LNAME').addClass('input-error'); }
				if (!email) { $('#mobile-menu #mc_embed_signup #mce-EMAIL').addClass('input-error'); }
				if (!country) { $('#mobile-menu #mc_embed_signup #mce-MMERGE5').addClass('input-error'); }
			}
			
			return false;
		});
		
	});