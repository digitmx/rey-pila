<?php get_header(); ?>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); $exclude_ids = array( $post->ID ); ?>

		<!-- NAVBAR -->
		<?php get_template_part("includes/navbar"); ?>
		
		<!-- HERO -->
		<?php $image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>
		<?php $mobile = get_field("mobile", $post->ID); if (!$mobile) { $mobile = $image; } ?>
		<div class="container-fluid" id="hero">
			<div class="parallax-container">
		  		<div class="parallax">
			  		<div class="cover"></div>
			  		<img class="hide-on-med-and-down" src="<?php echo $image; ?>">
			  		<img class="hide-on-large-only" src="<?php echo $mobile; ?>">
			  	</div>
			</div>
		  	<div id="caption">
			  	<div>
				  	<span class="din font70 white-text"><?php the_title(); ?></span>
			  	</div>
		  	</div>
		</div>
		
		<!-- CONTENT -->
		<div class="container" id="content">
			<div class="row">
				<div class="col s12 m12 l3" id="share">
					<div class="space40"></div>
					<a href="#" text="<?php the_title(); ?>" rel="<?php the_permalink(); ?>" class="waves-effect waves-light btn-flat btn-site-w helvetica font14 btnShareFacebook"><?php _e("SHARE FB","reypila_v1"); ?></a>
					<div class="space10"></div>
					<a href="#" text="<?php the_title(); ?>" rel="<?php the_permalink(); ?>" class="waves-effect waves-light btn-flat btn-site-w helvetica font14 btnShareTwitter"><?php _e("SHARE TW","reypila_v1"); ?></a>
				</div>
				<div class="col s12 m12 l9" id="post">
					<div class="space40"></div>
					<span class="helvetica font16 block"><?php the_content(); ?></span>
				</div>
			</div>
		</div>
		
		<?php endwhile; ?>
		<?php endif; ?>
		
		<hr>
		
		<!-- NEWS -->
      	<div class="container" id="news">
	      	<div class="row">
		      	<div class="col s12 m12 l12">
			      	<div class="space40"></div>
			      	<div class="centered">
				      	<span class="din font48 black-text"><?php _e("MORE REY PILA","reypila_v1"); ?></span>
			      	</div>
			      	<div class="space40"></div>
		      	</div>
		    </div>
	      	  
		    <?php 
			    //Query News
			    $args = array(
					'posts_per_page'   => 2,
					'order'			   => 'date',
					'orderby'          => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'post__not_in'	   => $exclude_ids,
					'suppress_filters' => false 
				);
				$posts_array = new WP_Query( $args ); 
			?>
			<?php foreach ($posts_array->posts as $posts) { //Proccess News ?>
		    <div class="row">
			    <div class="col s12 m12 l6">
				    <img class="responsive-img" src='<?php echo get_the_post_thumbnail_url( $posts->ID, $size = 'full' ); ?>'/>
				</div>
			  	<div class="col s12 m12 l6">
					<div class="space40"></div>
					<span class="din font24 black-text centeres"><?php echo $posts->post_title; ?></span>
		      	  	<div class="space40"></div>
		      	  	<span class="helvetica font14 black-text centered"><?php echo $posts->post_excerpt; ?></span>
		      	  	<div class="space40"></div>
		      	  	<a href="<?php echo get_the_permalink($posts->ID); ?>" class="waves-effect waves-light btn-flat btn-site-w helvetica font14"><?php _e("VIEW POST","reypila_v1"); ?></a>
		      	</div>
		    </div>
		    <?php wp_reset_postdata(); } ?>
		      	
	      	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
			      	<div class="centered">
				    	<a href="<?php bloginfo('url'); ?>/news/" class="waves-effect waves-light btn-flat btn-site-b helvetica font14"><?php _e("VIEW MORE","reypila_v1"); ?></a>
				    </div>
		      	</div>
	      	</div>
      	</div>
      	
      	<hr />
		
		<!-- CONTACTS -->
		<?php get_template_part("includes/contacts"); ?>

<?php get_footer(); ?>