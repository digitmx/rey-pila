<?php get_header(); ?>
		
		<?php get_template_part("includes/navbar"); ?>
		
		<div class="container-fluid">
			<div class="row">
				<div class="col s12 m12 l12">
					<?php $title = get_field("title_landing", "option"); ?>
					<?php $description = get_field("description_landing", "option"); ?>
					<div class="caption">
						<span class="din font70 white-text centered block"><?php echo $title; ?></span>
						<div class="space10"></div>
						<span class="helvetica font18 white-text centered block"><?php echo $description; ?></span>
					</div>
					<div class="cta">
						<div>
							<span class="font20 white-text block">
								<?php if (get_field("facebook_link", "option")) { ?>
								<a href="<?php the_field("facebook_link", "option"); ?>" target="_blank" class="icon">
									<svg id="facebook" data-name="facebook" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.5 293.7"><title>fb</title><path d="M99,293.7V159.7h45l6.7-52.2H99V74.2c0-15.1,4.2-25.4,25.9-25.4h27.6V2.1A370,370,0,0,0,112.2,0C72.4,0,45.1,24.3,45.1,69v38.5H0v52.2H45.1V293.7Z"/></svg>
								</a>
								<?php } ?>
								<?php if (get_field("twitter_link", "option")) { ?>
								<a href="<?php the_field("twitter_link", "option"); ?>" target="_blank" class="icon">
									<svg id="twitter" data-name="twitter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202.3 164.4"><title>tw</title><path d="M202.3,19.5A83,83,0,0,1,178.4,26,41.6,41.6,0,0,0,196.7,3a83,83,0,0,1-26.3,10.1A41.5,41.5,0,0,0,99.6,50.9,117.8,117.8,0,0,1,14.1,7.6,41.5,41.5,0,0,0,26.9,63,41.3,41.3,0,0,1,8.1,57.8c0,.2,0,.3,0,.5A41.5,41.5,0,0,0,41.4,99a41.6,41.6,0,0,1-18.7.7,41.5,41.5,0,0,0,38.8,28.8A83.3,83.3,0,0,1,9.9,146.3a84.1,84.1,0,0,1-9.9-.6,117.4,117.4,0,0,0,63.6,18.6c76.3,0,118.1-63.2,118.1-118.1q0-2.7-.1-5.4A84.3,84.3,0,0,0,202.3,19.5Z"/></svg>
								</a>
								<?php } ?>
								<?php if (get_field("instagram_link", "option")) { ?>
								<a href="<?php the_field("instagram_link", "option"); ?>" target="_blank"class="icon">
									<svg id="instagram" data-name="instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.2 43.2"><title>in</title><path d="M14.4,21.6a7.2,7.2,0,1,1,7.2,7.2,7.2,7.2,0,0,1-7.2-7.2m-3.9,0A11.1,11.1,0,1,0,21.6,10.5,11.1,11.1,0,0,0,10.5,21.6m20-11.5a2.6,2.6,0,1,0,2.6-2.6,2.6,2.6,0,0,0-2.6,2.6M12.9,39.2a11.9,11.9,0,0,1-4-.7,6.7,6.7,0,0,1-2.5-1.6,6.7,6.7,0,0,1-1.6-2.5,12,12,0,0,1-.7-4c-.1-2.3-.1-3-.1-8.7s0-6.4.1-8.7a12,12,0,0,1,.7-4A6.7,6.7,0,0,1,6.4,6.4,6.7,6.7,0,0,1,8.9,4.8a11.9,11.9,0,0,1,4-.7H30.3a12,12,0,0,1,4,.7,6.7,6.7,0,0,1,2.5,1.6,6.7,6.7,0,0,1,1.6,2.5,12,12,0,0,1,.7,4c.1,2.3.1,3,.1,8.7s0,6.4-.1,8.7a12,12,0,0,1-.7,4,7.2,7.2,0,0,1-4.1,4.1,11.9,11.9,0,0,1-4,.7H12.9M12.7.1a15.8,15.8,0,0,0-5.2,1A10.6,10.6,0,0,0,3.6,3.6,10.6,10.6,0,0,0,1.1,7.4a15.8,15.8,0,0,0-1,5.2C0,15,0,15.7,0,21.6s0,6.6.1,8.9a15.8,15.8,0,0,0,1,5.2,10.6,10.6,0,0,0,2.5,3.8,10.6,10.6,0,0,0,3.8,2.5,15.8,15.8,0,0,0,5.2,1H30.5a15.8,15.8,0,0,0,5.2-1,11,11,0,0,0,6.3-6.3,15.8,15.8,0,0,0,1-5.2c.1-2.3.1-3,.1-8.9s0-6.6-.1-8.9a15.8,15.8,0,0,0-1-5.2,10.6,10.6,0,0,0-2.5-3.8,10.6,10.6,0,0,0-3.8-2.5,15.8,15.8,0,0,0-5.2-1H12.7"/></svg>
								</a>
								<?php } ?>
								<?php if (get_field("youtube_link", "option")) { ?>
								<a href="<?php the_field("youtube_link", "option"); ?>" target="_blank"class="icon">
									<svg id="youtube" data-name="youtube" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1021.4 718.4"><title>yt</title><path id="The_Sharpness" data-name="The Sharpness" class="cls-1" d="M407,206,649,367.6,683,350Z" transform="translate(-1.8 -1.3)"/><g id="Lozenge"><path d="M1013,156.3s-10-70.4-40.6-101.4C933.6,14.2,890,14,870.1,11.6,727.1,1.3,512.7,1.3,512.7,1.3h-.4s-214.4,0-357.4,10.3C135,14,91.4,14.2,52.6,54.9,22,85.9,12,156.3,12,156.3S1.8,238.9,1.8,321.6v77.5C1.8,481.8,12,564.4,12,564.4s10,70.4,40.6,101.4c38.9,40.7,89.9,39.4,112.6,43.7,81.7,7.8,347.3,10.3,347.3,10.3s214.6-.3,357.6-10.7c20-2.4,63.5-2.6,102.3-43.3,30.6-31,40.6-101.4,40.6-101.4s10.2-82.7,10.2-165.3V321.6C1023.2,238.9,1013,156.3,1013,156.3ZM407,493V206L683,350Z" transform="translate(-1.8 -1.3)"/></g></svg>
								</a>
								<?php } ?>
								<?php if (get_field("spotify_link", "option")) { ?>
								<a href="<?php the_field("spotify_link", "option"); ?>" target="_blank"class="icon">
									<svg id="spotify" data-name="spotify" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 508.5 508.5"><title>sp</title><path d="M254.3,0C113.8,0,0,113.8,0,254.3S113.8,508.5,254.3,508.5,508.5,394.7,508.5,254.3,394.7,0,254.3,0ZM371.7,403.3a17.3,17.3,0,0,1-15.3,9.1,17.5,17.5,0,0,1-8.4-2.1,278.2,278.2,0,0,0-215.8-21.9,17.4,17.4,0,0,1-21.8-11.5c-2.9-9.2,2.3-28.7,11.5-31.6a313.2,313.2,0,0,1,243,24.7C373.3,374.5,376.4,394.9,371.7,403.3ZM404,307.5c-3.6,7-10.7,18.3-18.1,18.3a19.4,19.4,0,0,1-9.4-2.3,353.1,353.1,0,0,0-255.2-27,20.4,20.4,0,0,1-25-14.3c-2.9-10.9,3.5-29.4,14.3-32.4a393.7,393.7,0,0,1,284.6,30.3A20.3,20.3,0,0,1,404,307.5Zm13.5-76.7a23.4,23.4,0,0,1-10.4-2.4,428.2,428.2,0,0,0-192.8-45.2,436.3,436.3,0,0,0-104.5,12.7,23.3,23.3,0,0,1-28.2-17c-3.1-12.5,4.5-27.6,17-30.6a480.2,480.2,0,0,1,115.7-14.1A474.8,474.8,0,0,1,428,184.2a23.3,23.3,0,0,1,10.5,31.2C434.3,223.7,426.1,230.8,417.5,230.8Z"/></svg>
								</a>
								<?php } ?>
								<?php if (get_field("itunes_link", "option")) { ?>
								<a href="<?php the_field("itunes_link", "option"); ?>" target="_blank"class="icon">
									<svg id="itunes" data-name="itunes" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 90"><title>it</title><path id="iTunes" d="M45,0A45,45,0,1,0,90,45,45,45,0,0,0,45,0Zm0,81A36,36,0,1,1,81,45,36,36,0,0,1,45,81ZM61.5,57.1a7.4,7.4,0,0,1-6.4,7.8,6.6,6.6,0,0,1-7.5-6.1A7.4,7.4,0,0,1,54,51a6.8,6.8,0,0,1,3.4.4V32.6L39.4,36V59.5h0v.2A7.4,7.4,0,0,1,33,67.4a6.6,6.6,0,0,1-7.5-6.1,7.4,7.4,0,0,1,6.4-7.8,6.7,6.7,0,0,1,3.4.4V27h0l26.1-4.5h0V56.9h0Z"/></svg>
								</a>
								<?php } ?>
							</span>
							<br />
							</span>
							<a href="<?php bloginfo("url"); ?>/site" class="block waves-effect waves-light btn-flat din font24"><?php _e("ENTER SITE","reypila_v1"); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<?php get_footer(); ?>