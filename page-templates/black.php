<?php /* Template Name: Black Full Container */ ?>

<?php get_header(); ?>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<!-- NAVBAR -->
		<?php get_template_part("includes/navbar"); ?>
		
		<!-- HERO -->
		<?php $image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>
		<div class="container-fluid" id="hero">
			<div class="parallax-container">
		  		<div class="parallax">
			  		<div class="cover"></div>
			  		<img src="<?php echo $image; ?>">
			  	</div>
			</div>
		  	<div id="caption">
			  	<div>
				  	<span class="din font70 white-text"><?php the_title(); ?></span>
			  	</div>
		  	</div>
		</div>
		
		<!-- CONTENT -->
		<div class="container" id="content">
			<div class="row">
				<div class="col s12 m12 l12" id="post">
					<div class="space40"></div>
					<span class="helvetica font16 block"><?php the_content(); ?></span>
				</div>
			</div>
		</div>
		
		<?php endwhile; ?>
		<?php endif; ?>
      	
      	<hr />
		
		<!-- CONTACTS -->
		<?php get_template_part("includes/contacts"); ?>

<?php get_footer(); ?>