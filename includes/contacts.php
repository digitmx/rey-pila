		
		<?php $contacts = get_field("contacts_site", "option"); ?>
      	<div class="container" id="contact">
	    	<div class="row">
		    	<div class="col s12 m12 l12">
			    	<div class="space40"></div>
					<span class="din font48 black-text centered block"><?php _e("CONTACT","reypila_v1"); ?></span>
					<div class="space20"></div>
				</div>
	      	</div>
		  	<div class="row">
		    	<?php foreach ($contacts as $contact) { ?>
				<div class="col s12 m6 l6">
		      		<div class="row">
			  			<div class="col s12 m12 l4">
			  				<span class=" bold helvetica font16"><?php echo $contact['position']; ?></span>
			  			</div>
			  	  		<div class="col s12 m12 l8 ">
				  	  		<span class=" helvetica font16"><?php echo $contact['email']; ?></span>
				  	  	</div>
			  	 	</div>
		      	</div>
		      	<?php } ?>
	      	</div>
      	</div> 